Build with
```bash
mvn clean package
```

Run with
```bash
java -Djid=$JID -Dpassword=$XMPP_PASSWORD -jar target/smackgeolocdemo-1.jar
```
