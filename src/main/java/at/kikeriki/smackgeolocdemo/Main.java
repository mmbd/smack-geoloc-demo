package at.kikeriki.smackgeolocdemo;

import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.geoloc.GeoLocationManager;
import org.jivesoftware.smackx.geoloc.packet.GeoLocation;

class Main {
    public static void main(String[] args) throws Exception {
        System.out.println("Smack version " + SmackConfiguration.getVersion());
        SmackConfiguration.DEBUG = true;

        String jid = System.getProperty("jid");
        String password = System.getProperty("password");
		        
        AbstractXMPPConnection conn = new XMPPTCPConnection(jid, password);
        conn.connect().login();
		
        conn.setParsingExceptionCallback(stanzaData -> {
            stanzaData.getParsingException().printStackTrace();
        });
        GeoLocationManager geoLocationManager = GeoLocationManager.getInstanceFor(conn);
        geoLocationManager.addGeoLocationListener((from, geoevent, id, carrierMessage) -> {
            System.out.println(from.toString() + " is currently in " + geoevent.getRegion());
        });
        
        System.in.read();
        
        geoLocationManager.stopPublishingGeolocation();
        conn.disconnect();
	}
}
